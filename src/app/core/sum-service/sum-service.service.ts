import { Injectable } from "@angular/core";
import { HttpClient, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
  export class SumService {
    readonly prefix: string = 'add'
  
    constructor(private http: HttpClient) { }
  
    sum(num1: number, num2: number): Observable<number> {
        let model = {
            num1: num1,
            num2: num2
        };
      return this.http.post<number>(`https://localhost:7130/${this.prefix}`, model);
    }
  }