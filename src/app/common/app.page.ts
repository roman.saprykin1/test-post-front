import { Component, OnDestroy } from "@angular/core";
import { Subscription } from "rxjs";

@Component({
    template: ''
})
export class AppPage implements OnDestroy {
    private _subs: Subscription[] = [];

    set sub(value: Subscription | undefined) {
        if (value) {
            this._subs[this._subs.length] = value;
        }
    }

    ngOnDestroy(): void {
        this._subs.forEach(sub => sub?.unsubscribe());
    }
}