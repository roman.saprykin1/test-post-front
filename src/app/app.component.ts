import { Component } from '@angular/core';
import { SumService } from './core/sum-service/sum-service.service';
import { AppPage } from './common/app.page';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent extends AppPage {
  num1: number = 0;
  num2: number = 0;
  sum: number = 0;
  constructor(private sumService: SumService) {
    super();
  }

  onCalcSumClick() {
    if (!this.isNumber(this.num1)){
      alert("Wrong NUM1 number format");
      return;
    }

    if (!this.isNumber(this.num2)){
      alert("Wrong NUM2 number format");
      return;
    }

    this.sub = this.sumService.sum(this.num1, this.num2)
      .subscribe(result => {
        this.sum = result;
      });
  }

  isNumber(value?: string | number): boolean {
    return ((value != null) &&
      (value !== '') &&
      !isNaN(Number(value.toString())));
  }

}

